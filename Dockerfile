FROM debian:latest

FROM --platform=$BUILDPLATFORM debian:latest AS build
ARG TARGETPLATFORM
ARG BUILDPLATFORM
RUN echo "I am running on $BUILDPLATFORM, building for $TARGETPLATFORM"